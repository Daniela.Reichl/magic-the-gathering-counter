import React from "react";

type MonarchBtnProps = {
  playerName: string;
  onClick: () => void;
  isActive: boolean;
};

export const MonarchBtn = ({ onClick, isActive }: MonarchBtnProps) => {
  return (
    <img
      src="./src/assets/crown.png"
      alt="monarch"
      className={`border-2 ${isActive ? "active-monarch" : "inactive-monarch"}`}
      onClick={onClick}
    />
  );
};
