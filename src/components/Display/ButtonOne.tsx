import React from "react";

type ButtonOneProps = {
  incrementCounter1: () => void;
  decrementCounter1: () => void;
};

export const ButtonOne = ({
  incrementCounter1,
  decrementCounter1,
}: ButtonOneProps) => {
  return (
    <div>
      <div className="flex flex-col gap-5 text-center font-bold text-xl">
        <img
          src="./src/assets/plus.svg"
          className="btnCounter"
          onClick={incrementCounter1}
        ></img>
        <p className="text-3xl">1</p>
        <img
          src="./src/assets/minus.svg"
          className="btnCounter"
          onClick={decrementCounter1}
        ></img>
      </div>
    </div>
  );
};
