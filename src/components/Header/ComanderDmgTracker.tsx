import { useEffect, useState } from "react";

type ComanderDmgTrackerProps = {
  playerName: string;
};

export const ComanderDmgTracker = ({ playerName }: ComanderDmgTrackerProps) => {
  const storageDmg = `ComanderDmg ${playerName}`;
  const initialCounter = localStorage.getItem(storageDmg)
    ? parseInt(localStorage.getItem(storageDmg) || "0")
    : 0;

  const [counter, setCounter] = useState<number>(initialCounter);

  useEffect(() => {
    localStorage.setItem(storageDmg, counter.toString());
  }, [counter, storageDmg]);

  const pressDmgBtn = () => {
    setCounter(counter + 1);
  };

  return (
    <div
      className="flex items-center justify-center relative cursor-pointer"
      onClick={pressDmgBtn}
    >
      <img
        src="./src/assets/damage.svg"
        alt="comander damage"
        className="btnCounter opacity-50 z-0"
      />
      <span className="text-black font-bold text-3xl z-10 absolute  ">
        {counter}
      </span>
    </div>
  );
};
