import React, { useEffect, useState } from "react";

type ComanderCounterProps = {
  playerName: string;
};

export const ComanderCounter = ({ playerName }: ComanderCounterProps) => {
  const comanderCounter = `ComanderCounter ${playerName}`;
  const initialCounter = localStorage.getItem(comanderCounter)
    ? parseInt(localStorage.getItem(comanderCounter) || "0")
    : 0;

  const [counter, setCounter] = useState<number>(initialCounter);

  useEffect(() => {
    localStorage.setItem(comanderCounter, counter.toString());
  }, [counter, comanderCounter]);

  const pressDmgBtn = () => {
    setCounter(counter + 2);
  };

  return (
    <div
      className="flex items-center justify-center relative cursor-pointer"
      onClick={pressDmgBtn}
    >
      <img
        src="./src/assets/comander.png"
        alt="comander counter"
        className="btnCounter border-2 z-0"
      />
      <span className="counterText">{counter}</span>
    </div>
  );
};
