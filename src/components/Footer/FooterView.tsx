import React from "react";
import { PoisonBtn } from "./PoisonBtn";
import { MonarchBtn } from "./MonarchBtn";
import { ComanderCounter } from "./ComanderCounter";
import { Counter } from "./Counter";

type FooterViewProps = {
  playerName: string;
  handleMonarchClick: (monarchId: number) => void;
  isActive: boolean;
  monarchId: number;
};

export const FooterView = ({
  playerName,
  handleMonarchClick,
  isActive,
  monarchId,
}: FooterViewProps) => {
  return (
    <div className="flex justify-between">
      <PoisonBtn playerName={playerName} />
      <MonarchBtn
        playerName={playerName}
        onClick={() => handleMonarchClick(monarchId)}
        isActive={isActive}
      />
      <Counter playerName={playerName} />
      <ComanderCounter playerName={playerName} />
    </div>
  );
};
