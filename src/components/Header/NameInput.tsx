import { useState, useEffect } from "react";

type NameInputProps = {
  playerName: string;
};
export const NameInput = ({ playerName }: NameInputProps) => {
  const [isInputFocused, setInputFocused] = useState(false);
  const [inputValue, setInputValue] = useState("");

  const savedName = localStorage.getItem(playerName);

  useEffect(() => {
    // Set the initial input value based on savedName
    if (savedName) {
      setInputValue(savedName);
    }
  }, [savedName]);

  const handleButtonClick = () => {
    localStorage.setItem(`${playerName}`, inputValue);
    console.log(inputValue);
    setInputValue(inputValue);
    setInputFocused(false);
  };

  return (
    <div className="flex gap-5 items-center ">
      <input
        className="py-2 px-5 placeholder:text-slate-800 font-bold text-xl bg-transparent text-center w-36"
        type="text"
        placeholder="Dein Name"
        onFocus={() => setInputFocused(true)}
        value={inputValue}
        onChange={(e) => {
          setInputValue(e.target.value);
        }}
      />
      {isInputFocused && (
        <button className="bg-gray-500" onClick={handleButtonClick}>
          okay
        </button>
      )}
    </div>
  );
};
