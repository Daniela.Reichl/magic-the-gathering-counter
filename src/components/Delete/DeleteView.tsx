import React from "react";
import { DeleteBtn } from "./DeleteBtn";

export const DeleteView = () => {
  return (
    <div className="flex justify-center m-5">
      <DeleteBtn />
    </div>
  );
};
