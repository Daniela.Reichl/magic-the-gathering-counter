import React from "react";

type ButtonFiveProps = {
  incrementCounter5: () => void;
  decrementCounter5: () => void;
};

export const ButtonFive = ({
  incrementCounter5,
  decrementCounter5,
}: ButtonFiveProps) => {
  return (
    <div>
      <div className="flex flex-col gap-5 text-center font-bold text-xl">
        <img
          src="./src/assets/plus.svg"
          className="btnCounter"
          onClick={incrementCounter5}
        ></img>
        <p className="text-3xl">5</p>
        <img
          src="./src/assets/minus.svg"
          className="btnCounter"
          onClick={decrementCounter5}
        ></img>
      </div>
    </div>
  );
};
