import { useState, useEffect } from "react";
import { ButtonOne } from "./ButtonOne";
import { ButtonFive } from "./ButtonFive";

type DisplayViewProps = {
  playerName: string;
};

export const DisplayView = ({ playerName }: DisplayViewProps) => {
  const storageKey = `Counter ${playerName}`;

  const initialCounter = localStorage.getItem(storageKey)
    ? parseInt(localStorage.getItem(storageKey) || "40")
    : 40;

  const [counter, setCounter] = useState<number>(initialCounter);

  useEffect(() => {
    localStorage.setItem(storageKey, counter.toString());
  }, [counter, storageKey]);

  // Buttons left up and down
  const incrementCounter1 = () => {
    const updatedCounter = counter + 1;
    setCounter(updatedCounter);
  };

  const decrementCounter1 = () => {
    const updatedCounter = counter - 1;
    setCounter(updatedCounter);
  };

  // Buttons right up and down
  const incrementCounter5 = () => {
    const updatedCounter = counter + 5;
    setCounter(updatedCounter);
  };

  const decrementCounter5 = () => {
    const updatedCounter = counter - 5;
    setCounter(updatedCounter);
  };

  return (
    <div className="flex justify-between  py-10 items-center">
      <ButtonOne
        incrementCounter1={incrementCounter1}
        decrementCounter1={decrementCounter1}
      />
      <h1>{counter}</h1>
      <ButtonFive
        incrementCounter5={incrementCounter5}
        decrementCounter5={decrementCounter5}
      />
    </div>
  );
};
