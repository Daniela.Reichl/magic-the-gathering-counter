import React, { useEffect, useState } from "react";

type ComanderCounterProps = {
  playerName: string;
};

export const Counter = ({ playerName }: ComanderCounterProps) => {
  const rndCounter = `RndCounter ${playerName}`;
  const initialCounter = localStorage.getItem(rndCounter)
    ? parseInt(localStorage.getItem(rndCounter) || "0")
    : 0;

  const [counter, setCounter] = useState<number>(initialCounter);

  useEffect(() => {
    localStorage.setItem(rndCounter, counter.toString());
  }, [counter, rndCounter]);

  const pressDmgBtn = () => {
    setCounter(counter + 1);
  };

  return (
    <div
      className="flex items-center justify-center relative cursor-pointer"
      onClick={pressDmgBtn}
    >
      <img
        src="./src/assets/dice.webp"
        alt="random counter"
        className="btnCounter border-2 z-0"
      />
      <span className="counterText">{counter}</span>
    </div>
  );
};
