import { useState } from "react";

type ColorChangerProps = {
  onColorChange: (color: string) => void;
};

export const ColorChanger = ({ onColorChange }: ColorChangerProps) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleColorChange = (color: any) => {
    onColorChange(color);
    setIsOpen(false);
  };

  return (
    <div className="inline-block relative">
      <img
        src="./src/assets/farblos.png"
        alt="change color"
        className="h-10"
        onClick={toggleDropdown}
      />

      {isOpen && (
        <div className="absolute left-0 top-12 flex flex-col hover:bg-slate-300 border-2 border-gray-400">
          <img
            src="./src/assets/red.svg"
            className="BGColor red h-10"
            onClick={() => handleColorChange("red")}
          ></img>
          <img
            src="./src/assets/blue.svg"
            className="BGColor blue h-10"
            onClick={() => handleColorChange(`blue`)}
          ></img>
          <img
            src="./src/assets/white.svg"
            className="BGColor white h-10"
            onClick={() => handleColorChange(`white`)}
          ></img>
          <img
            src="./src/assets/green.svg"
            className="BGColor green h-10"
            onClick={() => handleColorChange(`green`)}
          ></img>
          <img
            src="./src/assets/black.svg"
            className="BGColor bg-gray-800 hover:bg-gray-600 h-10"
            onClick={() => handleColorChange(`black`)}
          ></img>
        </div>
      )}
    </div>
  );
};
