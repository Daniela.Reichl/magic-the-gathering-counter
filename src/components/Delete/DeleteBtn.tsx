import React from "react";

export const DeleteBtn = () => {
  const deleteAllStorage = () => {
    localStorage.clear();
    window.location.reload();
  };

  return (
    <div>
      <img
        src="./src/assets/clear.svg"
        alt="clear"
        className="btnCounter border-2 h-12"
        onClick={deleteAllStorage}
      />
    </div>
  );
};
