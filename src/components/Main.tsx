import React, { useState } from "react";
import { Header } from "./Header/Header";
import { DisplayView } from "./Display/DisplayView";
import { FooterView } from "./Footer/FooterView";
import { DeleteView } from "./Delete/DeleteView";

export const Main = () => {
  const [backgroundColors, setBackgroundColors] = useState([
    "bg-gray-200",
    "bg-gray-200",
  ]);
  const [activeMonarchs, setActiveMonarchs] = useState<(number | null)[]>([
    null,
    null,
  ]);

  const handleColorChange = (color: string, player: number) => {
    const newColors = [...backgroundColors];
    newColors[player - 1] = color;
    setBackgroundColors(newColors);
  };

  const handleMonarchClick = (player: number, monarchId: number) => {
    const newMonarchs = [...activeMonarchs];

    // If the monarch is already active, deactivate it
    if (newMonarchs[player - 1] === monarchId) {
      newMonarchs[player - 1] = null;
    } else {
      // Activate the clicked monarch and deactivate the other player's monarch
      newMonarchs[player - 1] = monarchId;
      const otherPlayer = player === 1 ? 2 : 1;
      newMonarchs[otherPlayer - 1] = null;
    }

    setActiveMonarchs(newMonarchs);
  };

  const players = [
    { playerName: "Spieler 1", playerNumber: 1 },
    { playerName: "Spieler 2", playerNumber: 2 },
  ];

  return (
    <div>
      {players.map((player) => (
        <div
          key={player.playerNumber}
          className={`border-t-2 border-black p-10 ${
            backgroundColors[player.playerNumber - 1]
          }`}
          style={{
            transform:
              player.playerNumber === 1 ? "rotate(180deg)" : "rotate(0deg)",
          }}
        >
          <Header
            onColorChange={(color) =>
              handleColorChange(color, player.playerNumber)
            }
            playerName={player.playerName}
          />
          <DisplayView playerName={player.playerName} />
          <FooterView
            playerName={player.playerName}
            handleMonarchClick={(monarchId) =>
              handleMonarchClick(player.playerNumber, monarchId)
            }
            isActive={
              activeMonarchs[player.playerNumber - 1] === player.playerNumber
            }
            monarchId={player.playerNumber}
          />
          <DeleteView />
        </div>
      ))}
    </div>
  );
};
