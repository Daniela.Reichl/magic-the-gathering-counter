import { ColorChanger } from "./ColorChanger";
import { ComanderDmgTracker } from "./ComanderDmgTracker";
import { NameInput } from "./NameInput";

type HeaderProps = {
  onColorChange: (color: string) => void;
  playerName: string;
};

export const Header = ({ onColorChange, playerName }: HeaderProps) => {
  return (
    <div className="flex gap-5 justify-between items-center">
      <ColorChanger onColorChange={onColorChange} />
      <NameInput playerName={playerName} />
      <ComanderDmgTracker playerName={playerName} />
    </div>
  );
};
