import { useEffect, useState } from "react";

type PoisonBtnProps = {
  playerName: string;
};

export const PoisonBtn = ({ playerName }: PoisonBtnProps) => {
  const storagePoison = `PoisonCnt ${playerName}`;
  const initialCounter = localStorage.getItem(storagePoison)
    ? parseInt(localStorage.getItem(storagePoison) || "0")
    : 0;

  const [counter, setCounter] = useState<number>(initialCounter);

  useEffect(() => {
    localStorage.setItem(storagePoison, counter.toString());
  }, [counter, storagePoison]);

  const pressDmgBtn = () => {
    setCounter(counter + 1);
  };

  return (
    <div
      className="flex items-center justify-center relative cursor-pointer"
      onClick={pressDmgBtn}
    >
      <img
        src="./src/assets/poison.svg"
        alt="poison"
        className="btnCounter border-2 border-white h-12 z-0"
      />
      <span className="counterText">{counter}</span>
    </div>
  );
};
